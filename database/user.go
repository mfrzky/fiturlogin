package database

type Users struct {
	ID         int64  `gorm:"column:id;primaryKey"`
	Username   string `gorm:"column:username;type:text"`
	Password   string `gorm:"column:password;"`
	KTP 	   string `gorm:"column:ktp;type:varchar(50)"`
	Pekerjaan  string `gorm:"column:pekerjaan;type:varchar(50)"`
	Nama 	   string `gorm:"column:nama;type:varchar(50)"`
	Pendidikan string `gorm:"column:pendidikan;type:varchar(20)"`
	Nomor	   int	  `gorm:"column:nomor;"`
}

func (user Users) TableName() string {
	return "users"
}
