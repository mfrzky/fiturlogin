package database

import "github.com/jinzhu/gorm"

func Migrate(DB *gorm.DB) {
	DB.AutoMigrate(&Users{})
}
