package main

import (
	"database/sql"
	"fmt"
	"net/http"

	"gitlab.com/login/database"
	"gitlab.com/login/tools"

	_ "github.com/lib/pq"
)

const (
	host     = "localhost"
	port     = 5432
	user     = "postgres"
	password = "admin"
	dbname   = "postgres"
  )

var db *sql.DB
var err error

type User struct {
	ID int
	Username string
	Password string
	KTP string
	Pekerjaan string
	Nama string
	Pendidikan string
	Nomor int
}

func connect_db() {
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
	"password=%s dbname=%s sslmode=disable",
	host, port, user, password, dbname)
  
	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}
	defer db.Close()

	err = db.Ping()
	if err != nil {
		panic(err)
	}

}

func routes()  {
	http.HandleFunc("/register", register)
}

func main() {
	ctx := tools.ServiceContext{}
	
	connect_db()
	database.Migrate(ctx.DB)

	routes()

	defer db.Close()

	fmt.Println("Server port :83")
	http.ListenAndServe(":83", nil)
}

func QueryUser(username string) User {
	var user = User{}
	db.QueryRow(`Select * FROM User Where Username=?`, username).Scan(
		&user.KTP,
		&user.Pekerjaan,
		&user.Nama,
		&user.Pendidikan,
		&user.Nomor,
	)
	return user
}
