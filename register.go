package main

import (
	"net/http"
	"golang.org/x/crypto/bcrypt"
)

func register(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.ServeFile(w, r, "html/register.gohtml")
		return
	}

	username := r.FormValue("email")
	password := r.FormValue("password")
	ktp := r.FormValue("ktp")
	pekerjaan := r.FormValue("pekerjaan")
	nama := r.FormValue("nama")
	pendidikan := r.FormValue("pendidikan")
	nomor := r.FormValue("nomor")

	users := QueryUser(username)

	if (User{}) == users {
		hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)

		if len(hashedPassword) != 0 {
			stmt, err := db.Prepare("INSERT INTO users SET username=?, password=?, ktp=?, pekerjaan=?, nama=?, pendidikan=?, nomor=?")
			if err == nil {
				_, err := stmt.Exec(&username, &hashedPassword, &ktp, &pekerjaan, &nama, &pendidikan, &nomor)
				if err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}

				http.Redirect(w, r, "/login", http.StatusSeeOther)
				return
			}
		}
	} else {
		http.Redirect(w, r, "/register", 302)
	}
}