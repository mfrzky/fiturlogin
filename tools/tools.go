package tools

import "github.com/jinzhu/gorm"

type ServiceContext struct {
	DB *gorm.DB
}